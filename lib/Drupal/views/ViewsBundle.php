<?php

/**
 * @file
 * Definition of Drupal\views\ViewsBundle.
 */

namespace Drupal\views;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Views dependency injection container.
 */
class ViewsBundle extends Bundle {
  /**
   * Array of plugin types to register.
   */
  protected $plugin_types = array(
    'access',
    'area',
    'argument',
    'argument_default',
    'argument_validator',
    'cache',
    'display',
    'display_extender',
    'exposed_form',
    'field',
    'filter',
    'localization',
    'pager',
    'query',
    'relationship',
    'row',
    'sort',
    'style',
    'wizard',
  );

  /**
   * Overrides Symfony\Component\HttpKernel\Bundle\Bundle.
   */
  public function build(ContainerBuilder $container)
    // register the ViewsPluginManager class with each plugin type as an argument.
    foreach ($this->plugin_types as $type) {
      $container->register('views.' . $type, 'Drupal\views\Plugin\Type\ViewsPluginManager')
        ->addArgument($type);
    }
  }

}
