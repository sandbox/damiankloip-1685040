<?php
/**
 * @file
 * Definition of Drupal\views\Plugin\views\wizard\WizardException.
 */

namespace Drupal\views\Plugin\views\wizard;

use Exception;

/**
 * A custom exception class for our errors.
 */
class WizardException extends Exception {
}
